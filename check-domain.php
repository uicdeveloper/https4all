<?php
/**
 * Created by PhpStorm.
 * User: Daniel-Paz-Horta
 * Date: 12/11/17
 * Time: 11:17 AM
 *
 * Quick N' Dirty script used to parse a CSV containing domain names and cUrl against them
 * to get the domain's headers and status code. Stores data into MySql.
 *
 */

// Include config file
include_once("config.php");

// Store Crawl results
$crawlResults = array();
$total = 0;
$domainTypes = "";

$crawlResults[] = crawlDomains("data/CNAME-2.csv", "CNAME");
$crawlResults[] = crawlDomains("data/ARecords-2.csv", "ALIAS");


// Report
echo "------------------------------------------" . PHP_EOL;

// Loop through the results placed into $crawlResults to derive totals for display
foreach($crawlResults as $result) {

    $total += $result['total'];
    $domainTypes = $domainTypes . ', ' . $result['domain_type'];

}

echo $total . " Crawled Domains (" . trim($domainTypes, ", ") . ")" . PHP_EOL . PHP_EOL;

/**
 * @param $csv_file_path
 * @param $domain_type
 */
function crawlDomains($csv_file_path, $domain_type)
{

    $csv = $csv_file_path;
    $spreadsheet = fopen($csv, "r", 1);

    $crawlData = array();

    // Iterator
    $x = 0;

    // Escape Flag
    $escape = false;

    // Loop
    while($domain = fgetcsv($spreadsheet, 0, ",")) {
        // Augment
        $x++;

        if(!empty($domain[0])) {

            try {

                $testData = testDomain($domain[0], $domain_type);

                if(is_array($testData) AND !empty($testData)){

                    $crawlData[] = $testData;
                    echo $domain[0] . PHP_EOL;

                }


            } catch (Exception $e){

                echo $e->getMessage() . PHP_EOL;

            }

        }

        // For Debugging
        if($x == 3 AND $escape){
            break;
        }

    }

    echo "------------------------------------------" . PHP_EOL;
    echo $x . " Crawled Domains (" . $domain_type . ")" . PHP_EOL . PHP_EOL;

    return array("domain_type" => $domain_type, "total" => $x);

}

/**
 * @param $domain
 * @param $domain_type
 * @return array
 */
function testDomain($domain, $domain_type)
{

    $data = array(
        'domain' => $domain,
        'http' => array(
            'code' => 0,
            'headers' => null
        ),
        'https' => array(
            'code' => 0,
            'headers' => null
        )
    );

    $protocols = array("http", "https");

    foreach($protocols as $k=>$protocol) {

        // Test the Domain for the respective protocol
        $responseData = curl($protocol, $domain);

        $data['domain_type'] = $domain_type;

        $data[$protocol]['code'] = $responseData['code'];

        $data[$protocol]['headers'] = json_encode($responseData);

    }

    // Save to the Database
    saveToDB($data);

    return $data;


}

/**
 * @param $protocol
 * @param $domain
 * @return array
 */
function curl($protocol, $domain)
{

    if(strtolower($protocol) != "http" AND strtolower($protocol) != "https" ){

        throw new Exception("invalid protocol");

    }

    // Initialized empty array
    $responseArray = array();

    // Set up a Curl Request
    $curl = curl_init();

    // Set curl options
    curl_setopt($curl,CURLOPT_USERAGENT, 'UniversityOfIllinoisChicagoHTTPSChecker/1.0 (https://aes.uic.edu/)');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, strtolower($protocol) . "://" . $domain);
    curl_setopt($curl, CURLOPT_HEADER, 1);
    curl_setopt($curl, CURLOPT_NOBODY, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);

    // JSON Response
    $response = curl_exec($curl);


    // Parse the $response into an array
    $responseArray = curlResponseHeadersArray($response);

    // Add on another element to $responseArray
    $responseArray = array_merge($responseArray, array("code" => curl_getinfo($curl, CURLINFO_RESPONSE_CODE)));

    // Close the curl resource
    curl_close($curl);

    // For JSON and XML requests
    return $responseArray;

}

/**
 * @param $response
 * @return array
 */
function curlResponseHeadersArray($response)
{
    $headers = array();

    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

    foreach (explode("\r\n", $header_text) as $i => $line)
        // The HTTP Code is not ': ' delimited, parse differently
        if ($i === 0) {

            $headers['http_code'] = $line;

        } else {

            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }

    return $headers;
}

/**
 * @param $data
 */
function saveToDB($data)
{

    $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
    $db = new PDO($dsn, DB_USER, DB_PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $db->prepare("INSERT IGNORE INTO webtech_gov_https (domain, domain_type, http, http_headers, https, https_headers, datestamp, timestamp, offset) VALUES(:domain,:domain_type, :http,:http_headers,:https,:https_headers, :datestamp, :timestamp, :offset)");
    $stmt->execute(array(
            ":domain" => $data['domain'],
            ":domain_type" => $data['domain_type'],
            ":http" => $data['http']['code'],
            ":http_headers" => $data['http']['headers'],
            ":https" => $data['https']['code'],
            ":https_headers" => $data['https']['headers'],
            ":datestamp"=> date('c', time()),
            ":timestamp"=> time(),
            ":offset"=> date('O', time())
        )
    );

}

/**
 * Creates the webtech_gov_https table
 */
function createDBTable()
{

    $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
    $db = new PDO($dsn, DB_USER, DB_PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $db->query("CREATE TABLE IF NOT EXISTS `webtech_gov_https` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(128) DEFAULT NULL,
  `domain_type` varchar(18) DEFAULT NULL,
  `http` int(11) DEFAULT NULL,
  `http_headers` mediumtext,
  `https` int(11) DEFAULT NULL,
  `https_headers` mediumtext,
  `datestamp` datetime DEFAULT NULL,
  `timestamp` varchar(36) DEFAULT NULL,
  `offset` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;)");
    $stmt->execute();

}