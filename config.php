<?php
/**
 * Created by PhpStorm.
 * User: Daniel-Paz-Horta
 * Date: 12/11/17
 * Time: 11:17 AM
 *
 */

// Constants for database connection
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'test');
define('DB_USER', 'YOUR_USER_NAME');
define('DB_PASS', 'YOUR_PASSWORD');